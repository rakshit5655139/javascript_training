function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}


function changeBackgroundColor() {
  const colorBox = document.querySelector(".color_box");
  const colorText = document.querySelector(".color_text");

  const randomColor = getRandomColor();
  colorBox.style.backgroundColor = randomColor;
  colorText.textContent = `Background Color: ${randomColor}`;
}


const colorButton = document.getElementById("color_button");
colorButton.addEventListener("click", changeBackgroundColor);