
// const createTodo = async (todo) => {
//   let options = {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json"
//     },
//     body: JSON.stringify(todo)
//   }


//   let p = await fetch('https://jsonplaceholder.typicode.com/posts', options)
//   let response = await p.json()
//   return response;

// }


// const mainFunc = async () => {
//   let todo = {
//     title: 'foo',
//     body: 'bar',
//     userId: 1,
//   }
//   let todor = await createTodo(todo)
//   console.log(todor)
// }

// mainFunc();



// document.getElementById('postForm').addEventListener('submit', function (e) {
//   e.preventDefault();

//   const postId = document.getElementById('postId').value;

//   // Make a POST request to fetch the post by its ID
//   fetch('https://jsonplaceholder.typicode.com/posts', {
//     method: 'POST',
//     body: JSON.stringify({ id: parseInt(postId) }),
//     headers: {
//       'Content-type': 'application/json; charset=UTF-8',
//     },
//   })
//     .then((response) => response.json())
//     .then((data) => {
//       // Display the post content
//       const postContent = document.getElementById('postContent');
//       postContent.innerHTML = `
//           <h2>Post Title: ${data.title}</h2>
//           <p>Post Body: ${data.body}</p>
//       `;
//     })
//     .catch((error) => {
//       console.error('Error:', error);
//     });
// });



document.getElementById('postForm').addEventListener('submit', function (e) {
  e.preventDefault();

  const postId = document.getElementById('postId').value;


  fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
    .then((response) => response.json())
    .then((data) => {


      const postContent = document.getElementById('postContent');
      postContent.innerHTML = `
          <p>Post userId: ${data.userId}</p>
          <h2>Post Title: ${data.title}</h2>
          <p>Post Body: ${data.body}</p>
      `;
    })
    .catch((error) => {
      console.error('Error:', error);
    });
});