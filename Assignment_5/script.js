let timer;
let hour = 0;
let minute = 0;
let second = 0;

const hoursDisplay = document.getElementById("hour");
const minutesDisplay = document.getElementById("minute");
const secondsDisplay = document.getElementById("second");

const startButton = document.getElementById("start");
const stopButton = document.getElementById("stop");
const resetButton = document.getElementById("reset");

startButton.addEventListener("click", startTimer);
stopButton.addEventListener("click", stopTimer);
resetButton.addEventListener("click", resetTimer);


function startTimer() {
  if (!timer) {
    timer = setInterval(updateTimer, 1000);
  }
}

function stopTimer() {
  clearInterval(timer);
  timer = null;
}

function resetTimer() {
  stopTimer();
  hour = 0;
  minute = 0;
  second = 0;
  updateDisplay();
}

function updateTimer() {
  second++;
  if (second === 60) {
    second = 0;
    minute++;
    if (minute === 60) {
      minute = 0;
      hour++;
    }
  }
  updateDisplay();
}


function updateDisplay() {
  hoursDisplay.textContent = (hour).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
  minutesDisplay.textContent = (minute).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
  secondsDisplay.textContent = (second).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
}









// function updateDisplay() {
//   hoursDisplay.textContent = padZero(hour);
//   minutesDisplay.textContent = padzero(minute);
//   secondsDisplay.textContent = padzero(second);
// }

// function padZero(value) {
//   return value.toString().padStart(2, "0");
// }